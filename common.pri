exists($$PWD/../XPlaneSDK) {
  XPLANE_SDK_PATH=$$PWD/../XPlaneSDK
}
exists($$(HOME)/SDK/) {
  XPLANE_SDK_PATH=$$(HOME)/SDK/
}

INCLUDEPATH += $$XPLANE_SDK_PATH/CHeaders/XPLM

unix:!macx {
    message("Linux Platform")
    DEFINES += APL=0 IBM=0 LIN=1
}
macx {
     message("Mac Platform")
     DEFINES += APL=1 IBM=0 LIN=0
}
win32 {
    message("Windows Platform")
    DEFINES += APL=0 IBM=1 LIN=0
}

# Build all the dataref classes as they are needed
# everywhere.

#SOURCES += \
#        $$PWD/server/datarefprovider.cpp \
#        ./server/datarefs/dataref.cpp \
#        ./server/datarefs/floatdataref.cpp \
#        ./server/datarefs/floatarraydataref.cpp \
#        ./server/datarefs/intdataref.cpp \
#        ./server/datarefs/doubledataref.cpp \
#        ./server/datarefs/intarraydataref.cpp \
#        ./server/datarefs/datadataref.cpp

#HEADERS += \
#        $$PWD/server/datarefprovider.h \
#        ./server/datarefs/dataref.h \
#        ./server/datarefs/floatdataref.h \
#        ./server/datarefs/floatarraydataref.h \
#        ./server/datarefs/intdataref.h \
#        ./server/datarefs/doubledataref.h \
#        ./server/datarefs/intarraydataref.h \
#        ./server/datarefs/datadataref.h
