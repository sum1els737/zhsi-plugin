#include <cstdlib>
#include <QDebug>
#include <XPLMPlugin.h>
#include <XPLMDataAccess.h>
#include <XPLMProcessing.h>
#include <XPLMUtilities.h>
#include "xplaneplugin.h"
#include "console.h"

static XPlanePlugin *globalPlugin = nullptr;


PLUGIN_API float MyFlightLoopCallback(
        float inElapsedSinceLastCall,
        float inElapsedTimeSinceLastFlightLoop,
        int inCounter,
        void *inRefcon) {
    if(globalPlugin)
        return globalPlugin->flightLoop(inElapsedSinceLastCall, inElapsedTimeSinceLastFlightLoop, inCounter, inRefcon);
    return 1;
}

PLUGIN_API int XPluginStart(
        char * outName,
        char * outSig,
        char *outDesc) {

    XPLMRegisterFlightLoopCallback(MyFlightLoopCallback, 0.01f, nullptr);
    Q_ASSERT(!globalPlugin);
    globalPlugin = new XPlanePlugin();
    if(globalPlugin) {
        return globalPlugin->pluginStart(outName, outSig, outDesc);
    } else {
        INFO << "Unable to create plugin";
        XPLMDebugString("ZHSI: Unable to create plugin\n");
        return 0;
    }
}
PLUGIN_API void XPluginStop() {
    DEBUG;
    XPLMDebugString("ZHSI: Stopping plugin\n");
    XPLMUnregisterFlightLoopCallback(MyFlightLoopCallback, nullptr);
    XPLMDebugString("ZHSI: FlightLoop Callback Unregistered\n");
    globalPlugin->pluginStop();
    //delete globalPlugin;
    //globalPlugin = nullptr;
    XPLMDebugString("ZHSI: Plugin stopped!\n");
}
PLUGIN_API void XPluginDisable() {
    DEBUG;
}
PLUGIN_API int XPluginEnable() {
    DEBUG;
    return 1;
}
PLUGIN_API void XPluginReceiveMessage(
        XPLMPluginID inFromWho,
        long inMessage,
        void *inParam) {
    if(globalPlugin)
        globalPlugin->receiveMessage(inFromWho, inMessage, inParam);
}
