include(../common.pri)

QMAKE_CXXFLAGS += -DXPLM301=1
QMAKE_CXXFLAGS += -DXPLM300=1
QMAKE_CXXFLAGS += -DXPLM210=1
QMAKE_CXXFLAGS += -DXPLM200=1

#INCLUDEPATH += $$XPLANE_SDK_PATH/CHeaders/XPLM
INCLUDEPATH += ../server
DEPENDPATH += . ../server
INCLUDEPATH += $$PWD/../util/
LIBS += -L../server -lserver

QT       += core network
QT       -= gui

CONFIG   += console warn_on shared c++11
CONFIG   -= app_bundle
CONFIG   -= debug_and_release

TEMPLATE = lib

TARGET = zhsi-plugin
QMAKE_LFLAGS += -shared -fPIC
#QMAKE_LFLAGS += ../server/libserver.a

unix:!macx {
    #message("Linux Platform")
    #DEFINES += APL=0 IBM=0 LIN=1
    QMAKE_CXXFLAGS += -rdynamic -nodefaultlibs -undefined_warning
    XPLDIR = zhsi_plugin/64
    XPLFILE = lin.xpl
    message("Plugin file will be copied to $$XPLDIR/$$XPLFILE")
    # Copy the built library to the correct x-plane plugin directory
    QMAKE_POST_LINK += $(MKDIR) $$XPLDIR ; $(COPY_FILE) $(TARGET) $$XPLDIR/$$XPLFILE
}
macx {
    #message("Mac Platform")
    #DEFINES += APL=1 IBM=0 LIN=0
    QMAKE_LFLAGS += -dynamiclib
    # -flat_namespace -undefined warning <- not needed or recommended anymore.

    # Build for multiple architectures.
    # The following line is only needed to build universal on PPC architectures.
    # QMAKE_MAC_SDK=/Devloper/SDKs/MacOSX10.4u.sdk
    # This line defines for wich architectures we build.
    CONFIG += x86 ppc
    QMAKE_LFLAGS += -F$$XPLANE_SDK_PATH/Libraries/Mac
    QMAKE_CXXFLAGS += -fPIC
    LIBS += -framework XPLM
    XPLDIR = zhsi_plugin/64
    XPLFILE = mac.xpl
    message("Plugin file will be copied to $$XPLDIR/$$XPLFILE")
    # Copy the built library to the correct x-plane plugin directory
    QMAKE_POST_LINK += $(MKDIR) $$XPLDIR ; $(COPY_FILE) $(TARGET) $$XPLDIR/$$XPLFILE
}
win32 {
    #DEFINES += APL=0 IBM=1 LIN=0
    DEFINES += NOMINMAX #Qt5 bug
    QMAKE_LFLAGS += -fPIC
    LIBS += -L$$XPLANE_SDK_PATH/Libraries/Win
    # We should test for target arch, not host arch, but this doesn't work. Fix.
    #    !contains(QMAKE_TARGET.arch, x86_64) {
    XPLFILE = win.xpl
    !contains(QMAKE_HOST.arch, x86_64) {
        message("Windows 32 bit Platform")
        LIBS += -lXPLM -lXPWidgets
        XPLDIR = zhsi_plugin\64
    } else {
        message("Windows 64 bit Platform")
        LIBS += -lXPLM_64 -lXPWidgets_64
        XPLDIR = zhsi_plugin\64
    }
    message("Plugin file will be copied to $$XPLDIR\\$$XPLFILE")
    # Copy the built library to the correct x-plane plugin directory
    QMAKE_POST_LINK += $(MKDIR) $$XPLDIR & $(COPY_FILE) $(TARGET) $$XPLDIR\\$$XPLFILE
}


CONFIG(debug, debug|release) {
    # Debug
    message("ZHSI Plugin Debug Build")
    debug.DESTDIR = $$DESTDIR
} else {
    # Release
    message("ZHSI Plugin Release Build")
    DEFINES += QT_NO_DEBUG
    DEFINES += QT_NO_DEBUG_OUTPUT
    release.DESTDIR = $$DESTDIR
}

SOURCES += \
        main.cpp \
        xplaneplugin.cpp \
        $$PWD/../server/tcpserver.cpp

HEADERS += \
        xplaneplugin.h \
        $$PWD/../server/tcpserver.h
