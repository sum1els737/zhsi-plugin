#include <clocale>
#include "console.h"
#include "xplaneplugin.h"
#include "datarefs/dataref.h"
#include "datarefs/dataref.h"
#include "datarefs/floatdataref.h"
#include "datarefs/floatarraydataref.h"
#include "datarefs/intdataref.h"
#include "datarefs/intarraydataref.h"
#include "datarefs/doubledataref.h"
#include "datarefs/datadataref.h"
#include <XPLMUtilities.h>
#include <XPLMDataAccess.h>
#include <XPLMMenus.h>
#include <stdio.h>

int zhsi_menu_container_idx;
static XPLMMenuID zhsi_menu_id = nullptr;
static XPLMDataRef zhsiPluginVersion = nullptr;
float getZhsiPluginVersion(void* inRefcon);

XPlanePlugin::XPlanePlugin(QObject *parent) : QObject(parent)
  , argc(0)
  , argv(nullptr)
  , app(nullptr)
  , server(nullptr)
  , flightLoopInterval(1.0f / 30.f) // Default to 30hz
{ }
XPlanePlugin::~XPlanePlugin() {
    DEBUG << Q_FUNC_INFO;
}
float XPlanePlugin::flightLoop(float inElapsedSinceLastCall,
                               float inElapsedTimeSinceLastFlightLoop,
                               int inCounter,
                               void *inRefcon) {
    Q_UNUSED(inElapsedSinceLastCall);
    Q_UNUSED(inElapsedTimeSinceLastFlightLoop);
    Q_UNUSED(inCounter);
    Q_UNUSED(inRefcon);
    // Tell each dataref to update its value through the XPLM api
    for(DataRef *ref : refs) updateDataRef(ref);

    // Tell Qt to process it's own runloop
    app->processEvents();
    return flightLoopInterval;
}
int XPlanePlugin::pluginStart(char * outName, char * outSig, char *outDesc) {
    INFO << "Plugin started";
    strcpy(outName, "ZHSI plugin");
    strcpy(outSig, "org.andreels.zhsi");
    strcpy(outDesc, "Plugin to communicate with ZHSI");

    zhsi_menu_container_idx = XPLMAppendMenuItem(XPLMFindPluginsMenu(), "ZHSI Plugin", nullptr, 0);
    zhsi_menu_id = XPLMCreateMenu("ZHSI Plugin", XPLMFindPluginsMenu(), zhsi_menu_container_idx, nullptr, nullptr);
    sprintf(msg, "ZHSI Plugin Version: %0.1f", VERSION);
    XPLMAppendMenuItem(zhsi_menu_id, msg, nullptr, 1);
    XPLMEnableMenuItem(zhsi_menu_id, 0, 0);

    // Init application and server
    app = new QCoreApplication(argc, &argv);
    setlocale(LC_NUMERIC, "C"); // See http://stackoverflow.com/questions/25661295/why-does-qcoreapplication-call-setlocalelc-all-by-default-on-unix-linux
    server = new TcpServer(this, this);

    connect(server, SIGNAL(setFlightLoopInterval(float)), this, SLOT(setFlightLoopInterval(float)));

    //register datarefs

    zhsiPluginVersion = XPLMRegisterDataAccessor("zhsi/plugin/version",
                                           xplmType_Float,
                                           0,
                                           nullptr, nullptr,
                                           getZhsiPluginVersion, nullptr,
                                           nullptr, nullptr,
                                           nullptr, nullptr,
                                           nullptr, nullptr,
                                           nullptr, nullptr,
                                           nullptr, nullptr);

    //init dataref

    zhsiPluginVersion = XPLMFindDataRef("zhsi/plugin/version");
    XPLMSetDataf(zhsiPluginVersion, VERSION);

    // Log that we have started
    sprintf(msg, "ZHSI: Plugin Version: %0.1f started\n", VERSION);
    //XPLMDebugString ("ZHSI: Plugin started\n");
    XPLMDebugString(msg);
    app->processEvents();
    return 1;
}
DataRef* XPlanePlugin::subscribeRef(QString &name) {
    DEBUG << name;

    // Search in list of already subscribed datarefs - if found return that
    for(DataRef *ref : refs) {
        if(ref->name() == name) {
            DEBUG << "Already subscribed to " << name;
            ref->setSubscriberCount(ref->subscriberCount() + 1);
            return ref;
        }
    }

    // Not yet subscribed - create a new dataref
    QString realName = refNameWithoutModifiers(name);
    XPLMDataRef ref = XPLMFindDataRef(realName.toLatin1());
    if(ref) {
        XPLMDataTypeID refType = XPLMGetDataRefTypes(ref);
        DataRef *dr = nullptr;
        if(refType & xplmType_Double) {
            dr = new DoubleDataRef(this, name, ref);
        } else if(refType & xplmType_Float) {
            dr = new FloatDataRef(this, name, ref);
        } else if(refType & xplmType_Int) {
            dr = new IntDataRef(this, name, ref);
        } else if (refType & xplmType_FloatArray) {
            dr = new FloatArrayDataRef(this, name, ref);
        } else if (refType & xplmType_IntArray) {
            dr = new IntArrayDataRef(this, name, ref);
        } else if (refType & xplmType_Data) {
            dr = new DataDataRef(this, name, ref);
        }
        if(dr) {
            dr->setSubscriberCount(1);
            dr->setWritable(XPLMCanWriteDataRef(ref));
            DEBUG << "Subscribed to ref " << dr->name()
                  << ", type: " << dr->typeString()
                  << ", writable:" << dr->isWritable();
            refs.append(dr);
            return dr;
        } else {
            INFO << "Dataref type " << refType << "not supported";
        }
    } else {
        INFO << "Can't find dataref " << name;
    }
    return nullptr;
}
void XPlanePlugin::unsubscribeRef(DataRef *ref) {
    Q_ASSERT(refs.contains(ref));
    DEBUG << ref->name() << ref->subscriberCount();

    ref->setSubscriberCount(ref->subscriberCount() - 1);
    if(ref->subscriberCount() == 0) {
        refs.removeOne(ref);
        DEBUG << "Ref " << ref->name() << " not subscribed by anyone - removing.";
        ref->deleteLater();
    }
}
// Called for each ref on every flight loop
void XPlanePlugin::updateDataRef(DataRef *ref)
{
    Q_ASSERT(ref);

    switch (ref->type()) {
    case zhsiRefTypeFloat:
    {
        float newValue = XPLMGetDataf(ref->ref());
        qobject_cast<FloatDataRef*>(ref)->updateValue(newValue);
        break;
    };
    case zhsiRefTypeFloatArray:
    {
        FloatArrayDataRef *faRef = qobject_cast<FloatArrayDataRef*>(ref);
        int arrayLength = faRef->value().length();
        if(arrayLength == 0) {
            arrayLength = XPLMGetDatavf(faRef->ref(), nullptr, 0, 0);
            faRef->setLength(arrayLength);
        }
        int valuesCopied = XPLMGetDatavf(faRef->ref(), faRef->valueArray(), 0, arrayLength);
        Q_ASSERT(valuesCopied == arrayLength);
        faRef->updateValue();
        break;
    };
    case zhsiRefTypeIntArray:
    {
        IntArrayDataRef *iaRef = qobject_cast<IntArrayDataRef*>(ref);
        int arrayLength = iaRef->value().length();
        if(arrayLength <= 0) {
            arrayLength = XPLMGetDatavi(iaRef->ref(), nullptr, 0, 0);
            iaRef->setLength(arrayLength);
        }
        int valuesCopied = XPLMGetDatavi(iaRef->ref(), iaRef->valueArray(), 0, arrayLength);
        Q_ASSERT(valuesCopied == arrayLength);
        iaRef->updateValue();
        break;
    };
    case zhsiRefTypeInt:
    {
        int newValue = XPLMGetDatai(ref->ref());
        qobject_cast<IntDataRef*>(ref)->updateValue(newValue);
        break;
    };
    case zhsiRefTypeDouble:
    {
        double newValue = XPLMGetDatad(ref->ref());
        qobject_cast<DoubleDataRef*>(ref)->updateValue(newValue);
        break;
    };
    case zhsiRefTypeData:
    {
        DataDataRef *bRef = qobject_cast<DataDataRef*>(ref);
        Q_ASSERT(bRef);
        int arrayLength = XPLMGetDatab(ref->ref(), nullptr, 0, 0);
        bRef->setLength(arrayLength);
        int valuesCopied = XPLMGetDatab(ref->ref(), bRef->newValue().data(), 0, arrayLength);
        Q_ASSERT(valuesCopied == arrayLength);
        bRef->updateValue();
        break;
    };
    default:
        break;
    }
}

void XPlanePlugin::keyStroke(int keyid) {
    DEBUG << keyid;
    XPLMCommandKeyStroke(keyid);
}

void XPlanePlugin::buttonPress(int buttonid) {
    DEBUG << buttonid;
    XPLMCommandButtonPress(buttonid);
}

void XPlanePlugin::buttonRelease(int buttonid) {
    DEBUG << buttonid;
    XPLMCommandButtonRelease(buttonid);
}

void XPlanePlugin::changeDataRef(DataRef *ref)
{
    if(!ref->isWritable()) {
        INFO << "Tried to write read-only dataref" << ref->name();
        return;
    }

    switch (ref->type()) {
    case zhsiRefTypeFloat:
    {
        XPLMSetDataf(ref->ref(), qobject_cast<FloatDataRef*>(ref)->value());
        break;
    }
    case zhsiRefTypeFloatArray:
    {
        FloatArrayDataRef *faRef = qobject_cast<FloatArrayDataRef*>(ref);
        XPLMSetDatavf(ref->ref(), faRef->valueArray(), 0, faRef->value().length());
        break;
    }
    case zhsiRefTypeIntArray:
    {
        IntArrayDataRef *iaRef = qobject_cast<IntArrayDataRef*>(ref);
        XPLMSetDatavi(ref->ref(), iaRef->valueArray(), 0, iaRef->value().length());
        break;
    }
    case zhsiRefTypeInt:
    {
        XPLMSetDatai(ref->ref(), qobject_cast<IntDataRef*>(ref)->value());
        break;
    }
    case zhsiRefTypeDouble:
    {
        XPLMSetDatad(ref->ref(), qobject_cast<DoubleDataRef*>(ref)->value());
        break;
    }
    default:
        break;
    }
}

void XPlanePlugin::command(QString &name, zhsiCommandType type)
{
    XPLMCommandRef cmdRef = XPLMFindCommand(name.toUtf8().constData());
    if (cmdRef) {
        switch (type) {
        case zhsiCommandTypeOnce:
            XPLMCommandOnce(cmdRef);
            break;
        case zhsiCommandTypeBegin:
            XPLMCommandBegin(cmdRef);
            break;
        case zhsiCommandTypeEnd:
            XPLMCommandEnd(cmdRef);
            break;
        default:
            break;
        }
    } else {
        INFO << "Command" << name << "not found";
    }

}
void XPlanePlugin::setFlightLoopInterval(float newInterval) {
    if(newInterval > 0) {
        flightLoopInterval = newInterval;
        DEBUG << "New interval" << flightLoopInterval;
    } else {
        DEBUG << "Invalid interval " << newInterval;
    }
}

QString XPlanePlugin::refNameWithoutModifiers(QString &original)
{
    return original.contains(":") ? original.left(original.indexOf(":")) : original;
}

/**
 * @brief XPlanePlugin::loadSituation
 * @param name :  situation file location -
 * relative to XPlane root folder, e.g. Output/situations/XXX.sit
 */
bool XPlanePlugin::loadSituation(QString sitFileLocation) {

    // Remove quotes from filename
    sitFileLocation = sitFileLocation.replace("\"", "");

    // XPLMLoadDataFile's return value is not documented, assuming it returns
    // 1 on success and 0 on fail. TODO: Check this.

    return XPLMLoadDataFile(xplm_DataFile_Situation, sitFileLocation.toUtf8().data());
}

void XPlanePlugin::pluginStop() {
    DEBUG;
    XPLMUnregisterDataAccessor(zhsiPluginVersion);
    app->processEvents();
    server->disconnectClients();
    //delete server;
    //server = nullptr;
    app->quit();
    app->processEvents();
    delete app;
    app = nullptr;
    qDeleteAll(refs);
    refs.clear();
}

void XPlanePlugin::receiveMessage(XPLMPluginID inFromWho, long inMessage, void *inParam) {
    Q_UNUSED(inParam);
    DEBUG << inFromWho << inMessage;
}
float getZhsiPluginVersion(void *inRefcon){
    Q_UNUSED(inRefcon);
    return VERSION;
}
