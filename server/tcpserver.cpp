#include "tcpserver.h"
#include "tcpclient.h"
#include <stdio.h>
#include "console.h"
#include <XPLMUtilities.h>

TcpServer::TcpServer(QObject *parent, DataRefProvider *refProvider) : QObject(parent)
    , _refProvider(nullptr)
    , _clientCount(0) {
    connect(&server, &QTcpServer::newConnection, this, &TcpServer::clientConnected);
    setDataRefProvider(refProvider);
}
TcpServer::~TcpServer() {
    disconnectClients();
}
int TcpServer::clientCount() const {
    return _clientCount;
}
void TcpServer::setDataRefProvider(DataRefProvider *refProvider)
{
    if(_refProvider) {
        disconnectClients();
    }
    _refProvider = refProvider;

    if(_refProvider) {
        if(!server.listen(QHostAddress::Any, TCP_PORT)) {
            sprintf(msg, "ZHSI: Unable to listen on port %d\n", TCP_PORT);
            XPLMDebugString(msg);
            return;
        }
        sprintf(msg, "ZHSI: Listening on port %d\n", TCP_PORT);
        XPLMDebugString(msg);
    } else {
        server.close();
    }
}
void TcpServer::clientConnected() {
    TcpClient *client = new TcpClient(this, server.nextPendingConnection(), _refProvider);
    connect(client, SIGNAL(discoed(TcpClient *)), this, SLOT(clientDiscoed(TcpClient *)));
    connect(client, SIGNAL(setFlightLoopInterval(float)), this, SIGNAL(setFlightLoopInterval(float)));
    clientConnections.append(client);
    _clientCount++;
    emit clientCountChanged(_clientCount);
}
void TcpServer::clientDiscoed(TcpClient *client) {
    clientConnections.removeOne(client);
    _clientCount--;
    emit clientCountChanged(_clientCount);
}

void TcpServer::disconnectClients()
{
    while (!clientConnections.isEmpty()) {
        TcpClient *client = clientConnections.takeLast();
        client->disconnect(this);
        client->deleteLater();
    }
    XPLMDebugString("ZHSI: Clients disconnected\n");
}
