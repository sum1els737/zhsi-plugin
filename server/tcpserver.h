#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QObject>
#include <QList>
#include <QTcpServer>
#include <QTcpSocket>

/**
  * Creates the TCP socket and manages client connections
  */
// TCP port used to listen for connections
#define TCP_PORT 52000
#define VERSION 0.1

class TcpClient;
class DataRefProvider;

class TcpServer : public QObject {
    Q_OBJECT
    Q_PROPERTY(int clientCount READ clientCount NOTIFY clientCountChanged)

public:
    TcpServer(QObject *parent=nullptr, DataRefProvider *refProvider=nullptr);
    ~TcpServer();
    int clientCount() const;
    void setDataRefProvider(DataRefProvider *refProvider);
    void disconnectClients();


signals:
    void setFlightLoopInterval(float newInterval);
    void clientCountChanged(int clientCount);

public slots:
    void clientConnected();
    void clientDiscoed(TcpClient *client);
private:
    char msg[200];
    int d = 0;
    QTcpServer server;
    QList<TcpClient *> clientConnections;
    DataRefProvider *_refProvider;
    int _clientCount;
};

#endif // TCPSERVER_H
