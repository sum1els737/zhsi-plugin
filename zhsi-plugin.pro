include(common.pri)

TEMPLATE = subdirs
SUBDIRS = server
CONFIG += ordered

defined(XPLANE_SDK_PATH, var) {
    message("Building X-Plane plugin with SDK in $$XPLANE_SDK_PATH")
    SUBDIRS += plugin
} else {
    warning("No X-Plane SDK found in ../XPlaneSDK or ~/SDK - not building X-Plane plugin")
}

OTHER_FILES += README.md
